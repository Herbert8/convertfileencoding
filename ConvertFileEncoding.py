# coding: UTF-8
import sys

def convertFile(oldFileName, newFileName, oldEncoding, newEncoding):
	fOld = open(oldFileName, 'r')
	fileContent = fOld.read().decode(oldEncoding).encode(newEncoding)
	fNew = open(newFileName, 'w')
	fNew.write(fileContent)
	fOld.close()
	fNew.close()

argList = sys.argv

if len(argList) < 3:
	# print 'Usage: python %s oldFileName, newFileName[, oldEncoding, newEncoding]' % argList[0]
	print '使用方法: python %s 旧文件, 新文件[, 旧编码, 新编码]' % argList[0]
	exit()

if len(argList) == 3:
	argList.append('gbk')
	argList.append('utf8')

if len(argList) == 4:
	argList.append('utf8')

convertFile(argList[1], argList[2], argList[3], argList[4])
print '转换成功！'
